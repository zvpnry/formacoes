
btnClickHandle=(e)=>{
    e.preventDefault();
    let nav = document.getElementsByClassName('nav')[0];
    nav.classList.toggle('active');
    

}

init = ()=>{
   
    let btnMenu = document.getElementsByClassName('nav-toggle')[0];
    btnMenu.addEventListener('click', btnClickHandle, false);
    let btnMenuClose=document.getElementsByClassName('nav-close')[0];    
    btnMenuClose.addEventListener('click',btnClickHandle,false);

    window.addEventListener('scroll',function(){
        let header = document.getElementsByClassName("header")[0];
        if(this.scrollY > 100)
        {
            header.classList.add('fixed');
        }
        else{
            header.classList.remove('fixed');
        }
    });
}


document.addEventListener('DOMContentLoaded',init,false);

import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit {
  
  stockSymbol = new FormControl();

  ngAfterViewInit(){

   this.stockSymbol.valueChanges.pipe(
     debounceTime(300)
   ).subscribe( stock => this.getStockFromServer(stock));

  }

  getStockFromServer (stock:string) {
    console.log(`O valor do Titulo ${stock} é : ${(Math.random()*100).toFixed(3)}`);
  }


}

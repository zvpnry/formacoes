import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit {
  
  @ViewChild('stockSymbol') stockSymbol:ElementRef;

  ngAfterViewInit(){

    let keyUp$ = fromEvent(this.stockSymbol.nativeElement, 'keyup');

    let keyUpValues$ = keyUp$.pipe(
      debounceTime(300),
      map(event => event['target'].value)
    ).subscribe(
      stock => this.getStockFromserver(stock)
    )

  }

  getStockFromserver (stock:string) {
    console.log(`O valor do Titulo ${stock} é : ${(Math.random()*100).toFixed(3)}`);
  }


}

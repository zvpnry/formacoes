import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
interface Product{
  id:String,
  title:string,
  price:Number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  products$:Observable<Product[]>;

  constructor(private http:HttpClient){
    let httpHeader = new HttpHeaders().set('Content-Type','application/json').set('Authorization','xpto 12345');
    let httpParams = new HttpParams().set('title','qualquer Coisa');
    this.products$ = this.http.get<Product[]>('/data/product.json',{
      headers:httpHeader,params:httpParams
    });
  }
}

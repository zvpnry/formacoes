Pasta de partilha do curso "Javascript Fundamentals" (30H), a decorrer na AGAP entre 7JAN2019 e 6FEV 2019

os meus contactos:
João Gonçalves
edu@joaogoncalves.net
http://joaogoncalves.net
Skype: joao_at_work

link de partilha:
http://bit.ly/agap-js


---------------------------------------------------
Pastas:
slides: 
serão colocados os slides apresentados em todas as sessoes, no final de cada sessão

ficheirosIniciais: 
ficheiros de partilha para iniciar um ou outro tema abordado nas sessoes

ficheirosFinais:
Ficheiros feitos pelo formador no final de cada sessão

---------------------------------------------------


PROGRAMA:
  JAVASCRIPT: INTRODUÇÃO À PROGRAMAÇÃO (30h) 
 
Objectivos:
Javascript é uma (senão a) linguagem de scripting, que em conjunto com HTML e CSS, constituem as tecnologias bases de desenvolvimento de Front-End para a Web (3 layers da web), que nos permite a criação de páginas e aplicações web interactivas e dinâmicas. 
 
Inicialmente desenvolvida como uma linguagem única e exclusivamente de Front-End, ou seja, interpretada pelo browser, atualmente existem especificações que nos permite a utilização desta linguagem tanto para “server-side” (nodeJS, ioJS, …), Base de Dados (couchDB, mongoDB, …), alem da possibilidade que nós oferece no desenvolvimento de aplicações de desktop e mobile nativas, e ainda muitas outras aplicações como são a Robótica, Arte, Internet of Things, Data Science, …
 
É uma linguagem “multi-paradigma”, podendo ser utilizada em ambientes de desenvolvimento Object Oriented (prototype based), Imperative ou Functional Programming.
 
Pretende-se com este curso dar uma formação sólida, atual e rigorosa em Javascript, introduzindo os conceitos básicos de desenvolvimento de páginas web dinâmicas e interactivas, ), utilização de boas praticas em programação, assim como a interação com tecnologia de “backend”, gestão de erros , esta abordagem seguirá os standards atuais, utilizando as novas ferramentas disponibilizadas a partir da versão ES6 (ECMAScript 2015), ES7 e ES8.
 
Conteúdo Programático:
* Introdução, história e evolução dos vários standards da linguagem.
* Tipos e estrutura de dados em Javascript (let, const, scope, ...)
* Estruturas de decisão e repetição em javascript
* Estruturas e estruturas de dados complexas como Funções, Arrays e Objectos. ( (Scope, IIFE’s, Closure’s, this & context, call / apply / bind, arrow functions, default parameters, Rest, parameters, Spread operators, destructuring Arrays e Objects, function generators, novos métodos de iteração em Arrays como map(), filter(), reduce(), entre outros)
* Classes e Modules em JS
* Conceito e manipulação do BOM (Browser Object Model) e DOM (Document Object Model) numa aplicação web.
* Trabalhar com Eventos em Javascript
* Comunicação com API’s do servidor através de chamadas assíncronas em AJAX e manipulação de dados em formato JSON, utilização de “Promises” com a “Fetch” API de HTML5.
* Gerir erros em Javascript, debug de aplicações.
 
Pré-Requisitos:
Conhecimentos sólidos em HTML, CSS e conceitos fundamentais de programação.
 
Destinatários:
Designers, Developers de outros ambientes de desenvolvimento, ou profissionais interessados em se iniciarem no desenvolvimento de aplicações web de acordo com as mais recentes tendencias, boas práticas e standards atuais.
 


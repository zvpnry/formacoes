//tudo o que estiver fora é dodefault sendo lib o nome que quiser dar para a o conteúdo do default
import lib,{dizerOla,nome,Pessoa} from "./libes5.js";

document.addEventListener('DOMContentLoaded',init,false);

function init()
{
    console.log("Teste");
    let info = document.getElementById('info');
    //Aceder ao Modulo de ES6
    info.innerHTML += `<p>Usando Module pattern em ES5, ${dizerOla()}</p>`;
    info.innerHTML += `<p>Usando Module pattern em ES5, ${lib()}</p>`;
    let micael = new Pessoa('Micael');
}
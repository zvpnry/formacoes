export const nome = 'Micael';
export const dizerOla =()=> `Olá ${nome} do Modulo de ES6`;


//apenas posso ter um export default
export default ()=> `O default da library de ES6`;


export class Pessoa{
    constructor(nome)
    {
        console.log(`Foi criada uma pessoa com o nome ${nome}`)
    }
}

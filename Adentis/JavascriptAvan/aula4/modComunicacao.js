class comunicacao
{
	constructor(url)
	{
		this.url = url;
	}
	/**Obter resposta em json assincronamente
	**/
	async obterJSON () {
		// esperar pela resposta da chamada do API fetch
		let resposta = await fetch(this.url);
		// Apenas continuar depois de resolvida a promessa
		let dados = await resposta.json();
		// Apenas continuar depois de resolvida a segunda promessa
		return dados;
	}
}
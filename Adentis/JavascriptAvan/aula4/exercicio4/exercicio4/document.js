﻿const _ = new class _ {
    constructor() {
        this.d = document;
    }
    /**
     * Pseudónimo do método document.getElementById()
     * @param {texto} n Nome do ID
     * @param {elemento} o OPCIONAL Objeto pai a partir do qual se faz a pesquisa, Por defeito é objeto document
     * @returns {elemento|nulo}
     */
    I(n = "", o = this.d) {
        return this.d.getElementById((typeof n === "string" ? n : ""));
    }
    /**
     * Pseudónimo do método document.getElementsByTagName()
     * @param {texto} n Nome do marcador
     * @param {elemento} o OPCIONAL Objeto pai a partir do qual se faz a pesquisa, Por defeito é objeto document
     * @returns {coleção|nulo}
     */
    T(n = "", o = this.d) {
        return this.d.getElementsByTagName((typeof n === "string" ? n : ""));
    }
};
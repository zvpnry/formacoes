﻿export class comunicacao {
 /**
* @class Api handles comunications with the server
* @param {url} obrigatório - localização para obtençao da informação
*/

    constructor(url) {
        this.url = url;
    }
    /**
    * @function obterJson handles a request to get data
    * @returns {null/JSON}
    */
    async obterJSON() {
        // esperar pela resposta da chamada do API fetch
        let r = await fetch(this.url);
        // Apenas continuar depois de resolvida a promessa
        
        return await r.json();
    }
	
    
}
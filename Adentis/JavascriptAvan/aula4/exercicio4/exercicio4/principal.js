﻿import { comunicacao } from './api.js';
class ManipulaInformacao {
    constructor(data, numeroLinhas=10) {
        this._conteudo = data;
        this._numeroLinhas = numeroLinhas;
    }    
    displayInList() {
        let HTML = '<ul>';
        for (var i = 0; i < this._numeroLinhas; i++) {
            HTML += `<li><p>${this._conteudo[i].id} - ${this._conteudo[i].title}</p></li>`;
        }
        HTML += '</ul>'
        return HTML;//JSON.stringify(this.content);
    }
    displayInTable() {
        let HTML = '<table><tr><th>ID</th><th>Title</th></tr>';
        for (var i = 0; i < this._numeroLinhas; i++) {
            HTML += `<tr><td>${this._conteudo[i].id}</td><td>${this._conteudo[i].title}</td></tr>`;
        }
        HTML += '</Table>'
        return HTML;//JSON.stringify(this.content);
    }
    showInLog() {
        for (var i = 0; i < this._numeroLinhas; i++) {
            console.log(`id:${this._conteudo[i].id} - Title: ${this._conteudo[i].title}`);
        }
        return '<p>Impresso na consola</p>';
    }
};




var obterVista = (type, dados, numberofRows) => {
    var m = new ManipulaInformacao(dados, 10);
    switch (type) {
        case "0"://allType
            m.showInLog();
            return m.displayInList() + m.displayInTable();
            break;
        case "1"://Just Table
            return m.displayInTable();
            break;
        case "2"://JustList
            return m.displayInList();
            break;
        case "3"://console
            return m.showInLog();
            break;
    }


};


function windowLoad() {
    init(10, "0");
}

function init(numberofRows, type = "0") {
    var com = new comunicacao("https://jsonplaceholder.typicode.com/posts/");
    com.obterJSON()
        .then(dados => {

            document.getElementById("onde").innerHTML = obterVista(type, dados, numberofRows);

        })
        .catch(erro => {
            document.getElementById("onde").innerHTML = "Erro";//erro.message;
        })
}

function async1() {
    let type = document.getElementById("typeElement").value;

    init(10, type);
}

function clear1() {
    document.getElementById("onde").innerHTML = '';
}

window.addEventListener("load", windowLoad);
document.getElementById('async').addEventListener("click", async1);
document.getElementById('clear').addEventListener("click", clear1)


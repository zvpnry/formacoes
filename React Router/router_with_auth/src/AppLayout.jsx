import React from 'react';
import auth from './auth.js'

const AppLayout = (props) => {
	return (
		<div>
			<div>App Layout</div>
			<button onClick={() => auth.logout(() => {
				props.history.push('/');
			})}>Logout</button>
		</div>);
}

export default AppLayout;
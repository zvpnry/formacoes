import React from 'react';
import LandingPage from './LandingPage.jsx';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AppLayout from './AppLayout.jsx';
import { ProtectedRoute } from './ProtectedRoute.jsx';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <h1>Protected React Router</h1>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <ProtectedRoute exact path="/app" component={AppLayout} />
          <Route component={() => '404 Not Found'} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;

const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const miniCSSExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
	entry: path.join(__dirname, 'src', 'app'),
	output: {
		path: path.join(__dirname, 'build'),
		filename: 'bundle.js',
	},
	plugins:[
		// copia o código html para a pasta build
		new htmlWebpackPlugin({
			template:'./src/index.html'
		}),
		new miniCSSExtractPlugin({
			filename:'bundle.css'
		})
	],
	devServer:{
		port:5001
	},
	module:{
		rules:[
			/* {
				test:/\.css$/,
				use: [
					{loader: 'style-loader'},
					{loader: 'css-loader'},
				]
			} */

			{
				test:/\.scss$/,
				use: [
					{loader: miniCSSExtractPlugin.loader},
					{loader: 'css-loader'},
					{loader: 'sass-loader'},
				]
			}
		]
	}
}
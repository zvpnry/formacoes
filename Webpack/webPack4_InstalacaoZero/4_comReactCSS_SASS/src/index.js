import React from 'react';
import ReactDOM from 'react-dom';
import APP from './App.js';
import './assets/facebook.ico';
import './fonts/austere.ttf';
import './fonts/GistLight.otf';
import './scss/global.scss';

ReactDOM.render(<APP />, document.getElementById('root'));
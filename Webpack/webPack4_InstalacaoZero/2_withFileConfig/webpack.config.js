const path = require('path');
module.exports = {
	mode: 'development', // pode ser enviado por comando como se pode ver no package.json. O que está no package.json vai sobrepor o que está neste ficheiro
	entry: path.resolve(__dirname, 'src', 'index.js'), // Ponto de entrada da aplicação
	/* Pode-se entrar vários pontos de entrada
	entry: {
		main: path.resolve(__dirname,'src','index.js'),
		admin: path.resolve(__dirname,'src','admin.js'),
	},
	*/

	output: {
		filename: 'bundle.js', //nome do ficheiro final, default é main.js
		path: path.resolve(__dirname, 'dist'), // caminho onde vai guardar o ficheiro
		publicPath: '/' // caminho que irá estar disponível todos os ficheiros do servidor quando se arranca com o webpack server 
	},
	module: {
		rules: [
			{
				test: /\.js$/, // Teste é uma expressão regular e é utilizada em cada import que é feito, é validado se o nome coincide com o que está na expressão regular
				loader: 'babel-loader', // caso não tenha opções senão pode-se usar o use 
				/* use: [
					{
						loader: 'babel-loader',
						options: {

						}
					}
				] */
				exclude: /node_modules/, // exclui a pasta node_modules para não voltar a compilar um código compilado, é uma expressão regular
				//include: //apenas os arquivos que passam neste teste são incluídos
			}
		]
	},
	devServer: { //especificar quando estamos em desenvolvimento o que queremos fazer
		port:3000, //infica porta de entrada da aplicação,
		contentBase: path.resolve(__dirname, 'src'), // indicar ponto de entrada, mas apenas necessária caso tenha vários pontos de entrada-> saber mais sobre isto
		proxy: { //por exemplo não resolver a url /user ou /admin, deixar isso para a aplicação, mas esta parte é opcional
			'/**':{ // todas a urls vão ser resolvidas dentro da index
				target: 'index.html',
				secure: false, // permitir https ou não
				bypass(req) {
					if(req.headers.accept.indexOf('html') !== -1) {
						return '/index.html';
					}
					return '';
				}
			}

		}

	},
	devtool: 'source-map' // se queremos ver os ficheiros no formato original ou não, ou seja no debugger com none não conseguimos obter o ficheiro index.js
	// source-map já conseguimos procurar pelos ficheiros e fazer debug
};

import React from 'react';
import ReactDOM from 'react-dom';
import APP from './app.js';

ReactDOM.render(<APP />, document.querySelector('[data-js="app"]'))